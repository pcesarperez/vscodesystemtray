; VSCodeSystemTrayControl.ahk
;
; Shows and hides Visual Studio Code with a single keystroke.
; The editor can be shown or hidden clicking on the System Tray icon as well.


#SingleInstance, force
#WinActivateForce
#Include VisualStudioCodeProcess.ahk


; Initial setup.
SetupSystemTray()
vsCodeRunningInstance := new VisualStudioCodeProcess(RunVSCodeIfNotPresent())
newFileLoadedChecker := Func("UnhideIfTitleHasChanged").bind(vsCodeRunningInstance)


; <kbd>F12</kbd> toggles the Visual Studio Code window status.
F12::
vsCodeRunningInstance := ToggleVSCodeWindowStatusOnKeyboard(vsCodeRunningInstance, newFileLoadedChecker)
return


; System Tray icon and default action behavior.
reloadOrToggleVSCode:
vsCodeRunningInstance := ToggleVSCodeWindowStatusOnSystemTrayIcon(vsCodeRunningInstance, newFileLoadedChecker)
return


; Sets up the System Tray to the active icon.
; It also defaults is behavior to the `Reload or toggle VSCode` action, even clicking on the icon.
SetupSystemTray() {
    SetActiveIcon()

    Menu, Tray, add
    Menu, Tray, add, Reload or toggle VSCode, reloadOrToggleVSCode
    Menu, Tray, Default, Reload or toggle VSCode
    Menu, Tray, Click, 1
}


; Checks if there is a running instance of Visual Studio Code.
; If there is not, it creates a new one.
;
; @return HWND of the Visual Studio Code instance, either existent or freshly minted.
RunVSCodeIfNotPresent() {
    Process, Exist, Code.exe

    vsCodePid := ErrorLevel

    if !WinExist("ahk_pid" vsCodePid) {
        return LoadVSCode()
    } else {
        return WinExist()
    }
}


; Toggles the Visual Code window status according to certain rules.
; This function is used when the hotkey is pressed.
;
; These are the rules:
;   * If Visual Studio Code is not running, then it is started.
;   * If the editor window is visible and minimized, thet it is restored.
;   * If the editor window is visible and not active, then it is activated.
;   * If the editor window is visible and active, then it is hidden.
;   * If the editor window is hidden, then it is unhidden.
;
; @param vsCodeRunningInstance The current running instance of Visual Studio Code.
; @param newFileLoadedChecker Reference to a function which checks if there is a new file loaded in the editor.
;
; @return Current running instance of Visual Studio Code, either existent or freshly minted.
ToggleVSCodeWindowStatusOnKeyboard(vsCodeRunningInstance, newFileLoadedChecker) {
    if (!vsCodeRunningInstance or !vsCodeRunningInstance.IsAlive) {
        vsCodeRunningInstance := new VisualStudioCodeProcess(LoadVSCode())
        SetActiveIcon()
    } else {
        if (vsCodeRunningInstance.IsVisible) {
            if (vsCodeRunningInstance.IsMinimized) {
                vsCodeRunningInstance.Restore()
            } else {
                if (!vsCodeRunningInstance.IsActive) {
                    vsCodeRunningInstance.Activate()
                } else {
                    vsCodeRunningInstance.Hide()
                    SetInactiveIcon()
                    StartHiddenTimer(newFileLoadedChecker)
                }
            }
        } else {
            vsCodeRunningInstance.Unhide()
            SetActiveIcon()
            StopHiddenTimer(newFileLoadedChecker)
        }
    }

    return vsCodeRunningInstance
}


; Toggles the Visual Code window status according to certain rules.
; This function is used when the System Tray icon is clicked.
; We need two different functions because clicking on the tray icon modifies the active state of the window.
; So, with this function, we can hide or show the editor window, but we cannot activate it.
;
; These are the rules:
;   * If Visual Studio Code is not running, then it is started.
;   * If the editor window is visible and minimized, thet it is restored.
;   * If the editor window is visible and not active, then it is hidden.
;   * If the editor window is visible and active, then it is hidden.
;   * If the editor window is hidden, then it is unhidden.
;
; @param vsCodeRunningInstance The current running instance of Visual Studio Code.
; @param newFileLoadedChecker Reference to a function which checks if there is a new file loaded in the editor.
;
; @return Current running instance of Visual Studio Code, either existent or freshly minted.
ToggleVSCodeWindowStatusOnSystemTrayIcon(vsCodeRunningInstance, newFileLoadedChecker) {
    if (!vsCodeRunningInstance or !vsCodeRunningInstance.IsAlive) {
        vsCodeRunningInstance := new VisualStudioCodeProcess(LoadVSCode())
        SetActiveIcon()
    } else {
        if (vsCodeRunningInstance.IsVisible) {
            if (vsCodeRunningInstance.IsMinimized) {
                vsCodeRunningInstance.Restore()
            } else {
                vsCodeRunningInstance.Hide()
                SetInactiveIcon()
                StartHiddenTimer(newFileLoadedChecker)
            }
        } else {
            vsCodeRunningInstance.Unhide()
            SetActiveIcon()
            StopHiddenTimer(newFileLoadedChecker)
        }
    }

    return vsCodeRunningInstance
}


; Loads Visual Studio Code.
;
; @return HWND of the Visual Studio Code instance.
LoadVSCode() {
    Run, "%LOCALAPPDATA%/Programs/Microsoft VS Code/Code.exe"
    WinWait, ahk_exe Code.exe

    return WinExist()
}


; Starts a timer to check if a new file has been loaded in the editor.
StartHiddenTimer(newFileLoadedChecker) {
    SetTimer, %newFileLoadedChecker%, 500
}


; Stops the timer which checks if a new file has been loaded in the editor.
StopHiddenTimer(newFileLoadedChecker) {
    SetTimer, %newFileLoadedChecker%, Off
}


; Checks if a new file has been loaded in the editor.
;
; @param vsCodeRunningInstance The current running instance of Visual Studio Code.
;
; @return <code>true</code> if there is a new file loaded, or <code>false</code> otherwise.
IsThereNewFileLoaded(vsCodeRunningInstance) {
    if (vsCodeRunningInstance.TitleHasChanged) {
        MsgBox, New file loaded
    }
}


; Action to un-hide the Visual Studio Code window if its title has changed, which means a new file has been loaded.
;
; @param vsCodeRunningInstance The current running instance of Visual Studio Code.
UnhideIfTitleHasChanged(vsCodeRunningInstance) {
    if (vsCodeRunningInstance.TitleHasChanged) {
        SetTimer,, Off
        vsCodeRunningInstance.Unhide()
        SetActiveIcon()
    }
}


; Sets the inactive icon in the System Tray.
SetInactiveIcon() {
    Menu, Tray, Icon, VSCodeInactive.ico
}


; Sets the active icon in the System Tray.
SetActiveIcon() {
    Menu, Tray, Icon, VSCodeActive.ico
}