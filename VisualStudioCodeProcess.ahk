; Process which represents a Visual Studio Code running instance.
class VisualStudioCodeProcess {
    ; Creates a new Visual Studio Code process instance.
    ;
    ; @param hwnd HWND of the Visual Studio Code process, either existent or freshly minted.
    __New(hwnd) {
        this.hwnd := hwnd
        this.savedWindowTitle := this.GetCurrentProcessTitle()
    }


    ; Hides the running Visual Studio Code process.
    Hide() {
        WinHide, % "ahk_id" this.hwnd

        this.Update()
    }


    ; Unhides the running Visual Studio Code process.
    Unhide() {
        WinShow, % "ahk_id" this.hwnd
        WinActivate, % "ahk_id" this.hwnd

        this.Update()
    }


    ; Minimizes the running Visual Studio Code process.
    Minimize() {
        WinMinimize, % "ahk_id" this.hwnd
    }


    ; Restores the running Visual Studio Code process.
    Restore() {
        WinRestore, % "ahk_id" this.hwnd
        WinActivate, % "ahk_id" this.hwnd
    }


    ; Activates the running Visual Studio Code process.
    Activate() {
        WinActivate, % "ahk_id" this.hwnd
    }


    ; Updates the running Visual Studio Code process.
    Update() {
        this.savedWindowTitle := this.GetCurrentProcessTitle()
    }


    ; Gets the running Visual Studio Code process title.
    ;
    ; @return The running Visual Studio Code process title.
    GetCurrentProcessTitle() {
        DetectHiddenWindows, On
        WinGetTitle, title, % "ahk_id" this.hwnd
        DetectHiddenWindows, Off

        return title
    }


    ; Checks if the running Visual Studio Code process is visible.
    IsVisible {
        get {
            return DllCall("IsWindowVisible", "UInt", WinExist("ahk_id" this.hwnd))
        }
    }


    ; Checks if the running Visual Studio Code process is minimized.
    IsMinimized {
        get {
            WinGet, vsCodeWindowStatus, MinMax, % "ahk_id" this.hwnd

            return vsCodeWindowStatus = -1
        }
    }


    ; Checks if the running Visual Studio Code process is active.
    IsActive {
        get {
            return WinActive("ahk_id" this.hwnd)
        }
    }


    ; Checks if the previously attached running Visual Studio Code process exists.
    IsAlive {
        get {
            DetectHiddenWindows, On
            isAttachedVSCodeWindowPresent := WinExist("ahk_id" this.hwnd)
            DetectHiddenWindows, Off

            return isAttachedVSCodeWindowPresent
        }
    }


    ; Process id (HWND) of the running Visual Studio Code process.
    ProcessId {
        get {
            return this.hwnd
        }
    }


    ; Current title of the running Visual Studio Code process.
    CurrentProcessTitle {
        get {
            return this.GetCurrentProcessTitle()
        }
    }


    ; Saved title of the running Visual Studio Code process.
    SavedProcessTitle {
        get {
            return this.savedWindowTitle
        }
    }


    ; Checks it the running Visual Studio Code process title has changed.
    TitleHasChanged {
        get {
            return this.CurrentProcessTitle != this.SavedProcessTitle
        }
    }
}